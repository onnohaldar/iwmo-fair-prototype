---
regel:
  type: Technische regel
  code: TR306
---
## Documentatie 

ProductCode is in de Toewijzing optioneel, en dus niet altijd gevuld. Wanneer ProductCode gevuld is in de toewijzing, spreken we van een specifieke toewijzing (in tegenstelling tot een aspecifieke toewijzing waarbij alleen ProductCategorie gevuld is). In geval van een specifieke toewijzing (dus met ProductCode gevuld) moet in het declaratie- of factuurbericht in de Prestatie altijd diezelfde toegewezen ProductCode overgenomen worden. In geval van een aspecifieke toewijzing moet in het declaratie- of factuurbericht een ProductCode ingevuld worden die volgens de gehanteerde productcodelijst hoort bij de toegewezen ProductCategorie.



** Generereerd door `create_content.py` op 28 February, 2022**

