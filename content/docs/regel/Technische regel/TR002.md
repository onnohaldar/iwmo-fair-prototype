---
regel:
  type: Technische regel
  code: TR002
---
## Documentatie 

Geboortedatum is onbekend als DatumGebruik de waarde '3 (dag, maand en jaar onbekend; onbekende datum)' heeft en Datum de waarde '01-01-1900' heeft.



** Generereerd door `create_content.py` op 28 February, 2022**

