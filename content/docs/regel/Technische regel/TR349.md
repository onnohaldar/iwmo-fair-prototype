---
regel:
  type: Technische regel
  code: TR349
---
## Documentatie 

Het verzoek om wijziging bericht bevat alle actuele toegewezen producten van de client, hetzij in OngewijzigdProduct, hetzij in TeWijzigenProduct
Actuele toewijzingen zijn toewijzingen die op of na de huidige datum geldig zijn, of waarvan de ingangsdatum in de toekomst ligt. 
Alle actuele toewijzingen zijn terug te vinden, hetzij als OngewijzigdProduct, hetzij als TeWijzigenProduct.




** Generereerd door `create_content.py` op 28 February, 2022**

