---
regel:
  type: Technische regel
  code: TR326
---
## Documentatie 

Een actueel startbericht is een startbericht waarbij nog geen stopbericht is verstuurd met dezelfde combinatie van sleutelvelden (gekoppeld via het toewijzingnummer) en dat niet is verwijderd d.m.v. een startbericht met status aanlevering verwijderen (zie IV008).
De levering is dan nog niet gestopt, en er mag geen start gestuurd  worden. Pas nadat er een stopbericht is gekomen kan er opnieuw een startbericht voor het toegewezen product (dezelfde combinatie van sleutelvelden) gestuurd worden.  




** Generereerd door `create_content.py` op 28 February, 2022**

