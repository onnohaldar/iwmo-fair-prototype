---
regel:
  type: Invulinstructie
  code: IV055
---
## Documentatie 

Debet en credit prestaties kunnen in één declaratie- of factuurbericht worden aangeleverd. Hierbij geldt:
Een 1e debet prestatie en een identieke credit prestatie mogen niet in één bericht (declaratie/factuur) worden aangeleverd. Als de 1e debet en credit prestatie op één moment bekend zijn, dan horen die tegen elkaar weg te vallen en niet in één bericht te staan.
Een credit prestatie en een 2e debet prestatie kunnen desgewenst in hetzelfde bericht worden aangeleverd.

Totaal declaratie-/factuurbedrag
Het totale declaratie-/factuurbedrag van alle debet en credit prestaties in het bericht wordt ingevuld in DeclaratieFactuurTotaalBedrag in de header van het bericht. Hierbij worden debetbedragen opgeteld en creditbedragen afgetrokken.



** Generereerd door `create_content.py` op 28 February, 2022**

