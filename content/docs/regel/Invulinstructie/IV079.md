---
regel:
  type: Invulinstructie
  code: IV079
---
## Documentatie 

Indien er is toegewezen in uren en er wordt gedeclareerd in uren, kan het voorkomen dat de werkelijk geleverde zorg niet uit hele uren (60 minuten) bestaat. 
In dat geval wordt het volume rekenkundig afgerond (< 30 minuten = omlaag >= 30 minuten = omhoog), met een minimaal volume van 1 uur. 
Geleverde zorg per prestatie behorend bij een toewijzing wordt gesommeerd over de gehele declaratieperiode, waarbij de afronding pas plaats vindt op het totaal volume in de prestatieregel.

Dus indien er wekelijks 95 minuten worden geleverd en de prestatieperiode bevat 4 weken, dan wordt eerst het totaal over de prestatieperiode bepaald = 4 weken x 95 minuten =380 minuten 
Er wordt gedeclareerd in uren: 380 / 60 = 6 uur en 20 minuten. Afgerond is dit 6 uur. In het volume van de prestatie wordt 6 uur gevuld.




** Generereerd door `create_content.py` op 28 February, 2022**

