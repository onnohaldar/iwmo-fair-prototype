---
regel:
  type: Invulinstructie
  code: IV042
---
## Documentatie 

In het verzoek om toewijzing en in de toewijzing wordt de omvang van de te leveren ondersteuning opgegeven in het berichtelement Omvang, dat bestaat uit de elementen Volume, Eenheid en Frequentie. Deze elementen dienen in onderlinge samenhang gevuld te worden.

In het declaratie- of factuurbericht wordt de omvang van de geleverde ondersteuning tijdens de betreffende productperiode opgegeven in de berichtelementen GeleverdVolume en Eenheid. Deze elementen dienen in onderlinge samenhang gevuld te worden.

Als Eenheid de waarde 83 (Euro's) bevat, wordt in GeleverdVolume een bedrag gevuld in eurocent. Bijvoorbeeld 10000 is gelijk aan tienduizend eurocent oftewel 100 euro.

Als Eenheid een andere waarde dan 83 (Euro's) bevat, wordt GeleverdVolume gevuld met een geheel getal, zonder decimalen.

Voorbeelden

Voorbeeld 1A: 
In een inspanningsgerichte toewijzing wordt aangegeven dat een client recht heeft op 5 dagdelen per week ondersteuning. Omvang in de toewijzing wordt als volgt gevuld:
Volume: 5
Eenheid: 16 (Dagdeel (4 uur))
Frequentie: 2 (Per week)

Voorbeeld 2A: 
In een outputgerichte toewijzing wordt aangegeven dat een client ondersteuning krijgt voor een vast bedrag van 200 euro per maand. Omvang in de toewijzing wordt als volgt gevuld:
Volume: 20000
Eenheid: 83 (Euro's)
Frequentie: 4 (Per maand)

Voorbeeld 1B: 
In een declaratie wordt aangegeven dat in de afgelopen productperiode in totaal 20 dagdelen ondersteuning is geleverd. Dit wordt als volgt aangegeven in het declaratiebericht:
GeleverdVolume: 20
Eenheid: 16 (Dagdeel (4 uur))

Voorbeeld 2B: 
In een factuur wordt aangegeven dat in de afgelopen productperiode ondersteuning is geleverd volgens het vaste, afgesproken maandbedrag van 200 euro. Dit wordt als volgt aangegeven in het declaratiebericht:
GeleverdVolume: 20000
Eenheid: 83 (Euro's)




** Generereerd door `create_content.py` op 28 February, 2022**

