---
regel:
  type: Invulinstructie
  code: IV007
---
## Documentatie 

Van een client of relatie worden de achternaam, voorvoegsel en voornamen en/of voorletters gescheiden vastgelegd. Voor het vastleggen van de VolledigeNaam van een client geldt het volgende format: 
* De Geslachtsnaam wordt altijd vastgelegd. Deze bestaat uit de Naam en eventueel een Voorvoegsel; 
* De Partnernaam kan worden vastgelegd. Ook deze bestaat uit de Naam en eventueel een Voorvoegsel; 
* Voornamen kunnen worden vastgelegd, gescheiden door spaties; 
* Voorletters kunnen worden vastgelegd, aaneengesloten, zonder punten of spaties; 
* NaamGebruik geeft de gewenste aanspreekvorm aan. Hiermee wordt bij correspondentie de volgorde bepaald in het gebruik van de geslachtsnaam en de naam van de partner. Het vullen van NaamGebruik hangt dus af van hoe de client of relatie zijn/haar naam hanteert.



** Generereerd door `create_content.py` op 28 February, 2022**

