---
regel:
  type: Bedrijfsregel
  code: OP090
---
## Documentatie 

De verzendende partij van het heenbericht is verantwoordelijk voor het signaleren van het ontbreken van een retourbericht en dient actie te ondernemen.



** Generereerd door `create_content.py` op 28 February, 2022**

