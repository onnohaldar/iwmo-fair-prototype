---
regel:
  type: Bedrijfsregel
  code: OP287
---
## Documentatie 

Bij een volledige toekenning zal het ingediende bedrag in de som van de door de gemeente toegekende bedragen in de header van het factuurretourbericht worden meegenomen.
Bij een volledige afwijzing zal het toegekende bedrag in het factuurretourbericht gelijk zijn aan 0 (nul).



** Generereerd door `create_content.py` op 28 February, 2022**

