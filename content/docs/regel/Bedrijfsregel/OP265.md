---
regel:
  type: Bedrijfsregel
  code: OP265
---
## Documentatie 

Dit betekent dat een aanbieder altijd dezelfde declaratie-/factuurperiode hanteert naar dezelfde gemeente. Dit kan niet per product verschillen.



** Generereerd door `create_content.py` op 28 February, 2022**

