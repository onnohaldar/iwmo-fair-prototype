---
regel:
  type: Bedrijfsregel
  code: OP297
---
## Documentatie 

Indien bij de verwijzing van huisarts, Jeugdarts, Gecertificeerde instelling of Medisch specialist een zorgverlenerscode (AGB-code van het Zorgpartijtype Zorgverlener) is meegegeven, dient bij de verwijzing deze ZorgverlenerCode gevuld te worden. Indien de zorgaanbieder deze informatie niet heeft, mag worden volstaan met de naam van de verwijzer.





** Generereerd door `create_content.py` op 28 February, 2022**

