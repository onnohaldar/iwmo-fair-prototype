---
regel:
  type: Bedrijfsregel
  code: OP303
---
## Documentatie 

De aanbieder die een declaratie-antwoordbericht ontvangt kan op basis van de meegestuurde gegevens de reactie op de declaratie verwerken in haar systeem. Dit is vooral van belang wanneer van een ingediend declaratiebericht een deel van de ingediende prestaties niet wordt toegekend. In het declaratie-antwoordbericht worden alleen  de prestaties meegestuurd die zijn afgewezen. Daarbij wordt met een retourcode aangegeven wat de reden is dat de ingediende prestatie niet is toegekend. 
Toegekende prestaties
Toegekende prestaties worden niet opgenomen in het retourbericht. Alleen de som van de ingediende bedragen van de toegekende prestaties wordt in de berichtklasse DeclaratieAntwoord van het declaratie-antwoordbericht opgenomen. 



** Generereerd door `create_content.py` op 28 February, 2022**

