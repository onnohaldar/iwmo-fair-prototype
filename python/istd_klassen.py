# DataType:
# - kan meerdere elementen bevatten
# - kan worden beperkt door een codelijst
# - en er kunnen meerdere regels voor gelden
class IstdDataType:
    def __init__(self, naam: str):
        self.naam: str = naam
        self.beschrijving: str = []
        self.elementen: IstdElement = []
        self.codeLijst: str = None
        self.regels: str = []

# Element:
# - heeft een datatype
# - kan wel of geen sleutel zijn
# - en er kunnen meerdere regels voor gelden
class IstdElement:
    def __init__(self, naam: str):
        self.naam: str = naam
        self.beschrijving: str = []
        self.dataType: str = None
        self.waarde = None
        self.sleutel: bool = False
        self.regels: str = []

# Klasse:
# - bestaat uit meerdere elementen
# - en er kunnen meerdere regels voor gelden
class IstdKlasse:
    def __init__(self, naam: str):
        self.naam: str = naam
        self.beschrijving: str = []
        self.regels: str = []
        self.elementen: IstdElement = []

# Aggregatie Relatie
# - Is van parent-Klasse naar child-Klasse
# - Geeft Min - Max voorkomens van de child-Klasse aan (default = 1 - 1)
class IstdAggregatieRelatie:
    def __init__(self, naam: str):
        self.naam: str = naam
        self.parentKlasse: str = None
        self.childKlasse: str = None
        self.childMin: str = '1'
        self.childMax: str = '1'

# Bericht:
# - Bestaat uit meerdere klassen 
# - Kunnen meerdere regels voor gelden
# - Meerdere aggregatie-relaties tussen klassen
class IstdBericht:
    def __init__(self, naam: str):
        self.naam: str = naam.upper()
        self.standaard = 'istd'
        self.release = 'x.y.z'
        self.berichtXsdVersie = 'x.y.z'
        self.berichtXsdMinVersie = 'x.y.z'
        self.berichtXsdMaxVersie = 'x.y.z'
        self.basisschemaXsdVersie = 'x.y.z'
        self.basisschemaXsdMinVersie = 'x.y.z'
        self.basisschemaXsdMaxVersie = 'x.y.z'
        self.beschrijving: str = []
        self.regels: str = []
        self.klassen: IstdKlasse = []
        self.relaties: IstdAggregatieRelatie = []

# Regel:
# - is uniek binnen de verzameling regels
# - is optioneel gekoppeld aan referentie-regel
class IstdRegel:
    def __init__(self):
        self.type: str = None
        self.code: str = None
        self.titel: str = None
        self.documentatie: str = None
        self.XSDRestrictie = None
        self.retourcode = None
        self.controleniveau = None
        self.referentieregel = None

