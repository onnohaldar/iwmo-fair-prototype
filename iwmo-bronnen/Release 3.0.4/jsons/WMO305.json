{
  "xs:schema": {
    "@xmlns:xs": "http://www.w3.org/2001/XMLSchema",
    "@xmlns:iwmo": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
    "@xmlns:wmo305": "http://www.istandaarden.nl/iwmo/3_0/wmo305/schema",
    "@targetNamespace": "http://www.istandaarden.nl/iwmo/3_0/wmo305/schema",
    "@elementFormDefault": "qualified",
    "xs:import": {
      "@namespace": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
      "@schemaLocation": "basisschema.xsd"
    },
    "xs:annotation": {
      "xs:appinfo": {
        "iwmo:standaard": "iwmo",
        "iwmo:bericht": "wmo305",
        "iwmo:release": "3.0",
        "iwmo:BerichtXsdVersie": "1.0.2",
        "iwmo:BerichtXsdMinVersie": "1.0.0",
        "iwmo:BerichtXsdMaxVersie": "1.0.2",
        "iwmo:BasisschemaXsdVersie": "1.2.0",
        "iwmo:BasisschemaXsdMinVersie": "1.0.0",
        "iwmo:BasisschemaXsdMaxVersie": "1.2.0"
      }
    },
    "xs:element": {
      "@name": "Bericht",
      "@type": "wmo305:Root"
    },
    "xs:complexType": [
      {
        "@name": "Root",
        "xs:annotation": {
          "xs:documentation": "Bericht voor het melden van de start van levering van Wmo-ondersteuning."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Header",
              "@type": "wmo305:Header"
            },
            {
              "@name": "Client",
              "@type": "wmo305:Client"
            }
          ]
        }
      },
      {
        "@name": "Header",
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "BerichtCode",
              "xs:annotation": {
                "xs:documentation": "Code ter identificatie van een soort bericht."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtCode",
                  "xs:pattern": {
                    "@value": "418"
                  }
                }
              }
            },
            {
              "@name": "BerichtVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een major release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtVersie",
                  "xs:pattern": {
                    "@value": "3"
                  }
                }
              }
            },
            {
              "@name": "BerichtSubversie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een minor release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtSubversie",
                  "xs:pattern": {
                    "@value": "0"
                  }
                }
              }
            },
            {
              "@name": "Afzender",
              "@type": "iwmo:LDT_AgbCode",
              "xs:annotation": {
                "xs:documentation": "Identificerende code van een aanbieder van zorg of ondersteuning."
              }
            },
            {
              "@name": "Ontvanger",
              "@type": "iwmo:LDT_Gemeente",
              "xs:annotation": {
                "xs:documentation": "Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of ondersteuning."
              }
            },
            {
              "@name": "BerichtIdentificatie",
              "@type": "iwmo:CDT_BerichtIdentificatie",
              "xs:annotation": {
                "xs:documentation": "Naam of nummer en dagtekening ter identificatie van een totale aanlevering."
              }
            },
            {
              "@name": "XsdVersie",
              "@type": "iwmo:CDT_XsdVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het heenbericht."
              }
            }
          ]
        }
      },
      {
        "@name": "Client",
        "xs:annotation": {
          "xs:documentation": "Persoonsgegevens van de client."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Bsn",
              "@type": "iwmo:LDT_BurgerServicenummer",
              "xs:annotation": {
                "xs:documentation": "Een door de overheid toegekend identificerend nummer in het kader van het vereenvoudigen van het contact tussen overheid en burgers en het verminderen van de administratieve lasten."
              }
            },
            {
              "@name": "Geboortedatum",
              "@type": "iwmo:CDT_Geboortedatum",
              "xs:annotation": {
                "xs:documentation": "Datum waarop een natuurlijk persoon geboren is."
              }
            },
            {
              "@name": "Geslacht",
              "@type": "iwmo:LDT_Geslacht",
              "xs:annotation": {
                "xs:documentation": "De sekse van een persoon, zoals bij geboorte formeel vastgesteld of nadien formeel gewijzigd."
              }
            },
            {
              "@name": "Naam",
              "@type": "iwmo:CDT_VerkorteNaam",
              "xs:annotation": {
                "xs:documentation": "De naam van een natuurlijk persoon, aangeduid als Geslachtsnaam en eventueel Voornamen en/of Voorletters."
              }
            },
            {
              "@name": "StartProducten",
              "@type": "wmo305:StartProducten"
            }
          ]
        }
      },
      {
        "@name": "StartProducten",
        "xs:sequence": {
          "xs:element": {
            "@name": "StartProduct",
            "@type": "wmo305:StartProduct",
            "@maxOccurs": "unbounded"
          }
        }
      },
      {
        "@name": "StartProduct",
        "xs:annotation": {
          "xs:documentation": "Gegevens over het product waarvan de levering gestart is."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "ToewijzingNummer",
              "@type": "iwmo:LDT_Nummer",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Identificerend nummer van de opdracht om een zorg - of ondersteuningsproduct te leveren, zoals vastgesteld door de gemeente."
              }
            },
            {
              "@name": "Product",
              "@type": "iwmo:CDT_Product",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Gecodeerde omschrijving van een product, dienst of resultaat ten behoeve van het leveren van ondersteuning aan een client."
              }
            },
            {
              "@name": "ToewijzingIngangsdatum",
              "@type": "iwmo:LDT_Datum",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "De datum waarop de toegewezen product voor de eerste keer geleverd dient te worden (gelijk aan de Ingangsdatum van het toegewezen product in het Toewijzingbericht)."
              }
            },
            {
              "@name": "Begindatum",
              "@type": "iwmo:LDT_Datum",
              "xs:annotation": {
                "xs:documentation": "Datum vanaf wanneer een ondersteuningsproduct wordt geleverd."
              }
            },
            {
              "@name": "StatusAanlevering",
              "@type": "iwmo:LDT_StatusAanlevering",
              "xs:annotation": {
                "xs:documentation": "Indicatie over de status van de informatie in de berichtklasse."
              }
            }
          ]
        }
      }
    ]
  }
}
