{
  "xs:schema": {
    "@xmlns:xs": "http://www.w3.org/2001/XMLSchema",
    "@xmlns:iwmo": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
    "@xmlns:wmo320": "http://www.istandaarden.nl/iwmo/3_0/wmo320/schema",
    "@targetNamespace": "http://www.istandaarden.nl/iwmo/3_0/wmo320/schema",
    "@elementFormDefault": "qualified",
    "xs:import": {
      "@namespace": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
      "@schemaLocation": "basisschema.xsd"
    },
    "xs:annotation": {
      "xs:appinfo": {
        "iwmo:standaard": "iwmo",
        "iwmo:bericht": "wmo320",
        "iwmo:release": "3.0",
        "iwmo:BerichtXsdVersie": "1.0.2",
        "iwmo:BerichtXsdMinVersie": "1.0.0",
        "iwmo:BerichtXsdMaxVersie": "1.0.2",
        "iwmo:BasisschemaXsdVersie": "1.2.0",
        "iwmo:BasisschemaXsdMinVersie": "1.0.0",
        "iwmo:BasisschemaXsdMaxVersie": "1.2.0"
      }
    },
    "xs:element": {
      "@name": "Bericht",
      "@type": "wmo320:Root"
    },
    "xs:complexType": [
      {
        "@name": "Root",
        "xs:annotation": {
          "xs:documentation": "Bericht voor antwoordinformatie over het Verzoek om toewijzing of Verzoek om wijziging Wmo-hulp."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Header",
              "@type": "wmo320:Header"
            },
            {
              "@name": "Antwoord",
              "@type": "wmo320:Antwoord",
              "@minOccurs": "0"
            }
          ]
        }
      },
      {
        "@name": "Header",
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "BerichtCode",
              "xs:annotation": {
                "xs:documentation": "Code ter identificatie van een soort bericht."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtCode",
                  "xs:pattern": {
                    "@value": "483"
                  }
                }
              }
            },
            {
              "@name": "BerichtVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een major release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtVersie",
                  "xs:pattern": {
                    "@value": "3"
                  }
                }
              }
            },
            {
              "@name": "BerichtSubversie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een minor release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtSubversie",
                  "xs:pattern": {
                    "@value": "0"
                  }
                }
              }
            },
            {
              "@name": "Afzender",
              "@type": "iwmo:LDT_Gemeente",
              "xs:annotation": {
                "xs:documentation": "Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of ondersteuning."
              }
            },
            {
              "@name": "Ontvanger",
              "@type": "iwmo:LDT_AgbCode",
              "xs:annotation": {
                "xs:documentation": "Identificerende code van een aanbieder van zorg of ondersteuning."
              }
            },
            {
              "@name": "BerichtIdentificatie",
              "@type": "iwmo:CDT_BerichtIdentificatie",
              "xs:annotation": {
                "xs:documentation": "Naam of nummer en dagtekening ter identificatie van een totale aanlevering."
              }
            },
            {
              "@name": "XsdVersie",
              "@type": "iwmo:CDT_XsdVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het heenbericht."
              }
            },
            {
              "@name": "IdentificatieRetour",
              "@type": "iwmo:LDT_IdentificatieBericht",
              "xs:annotation": {
                "xs:documentation": "Naam of nummer ter identificatie van een retourbericht."
              }
            },
            {
              "@name": "DagtekeningRetour",
              "@type": "iwmo:LDT_Datum",
              "xs:annotation": {
                "xs:documentation": "Dagtekening van het retourbericht."
              }
            },
            {
              "@name": "XsltVersie",
              "@type": "iwmo:LDT_Versie",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een versie van de XSLT's die zijn ingezet voor de controle van het heenbericht."
              }
            },
            {
              "@name": "XsdVersieRetour",
              "@type": "iwmo:CDT_XsdVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het retourbericht."
              }
            },
            {
              "@name": "RetourCodes",
              "@type": "wmo320:RetourCodes",
              "@minOccurs": "0"
            }
          ]
        }
      },
      {
        "@name": "RetourCodes",
        "xs:sequence": {
          "xs:element": {
            "@name": "RetourCode",
            "@type": "iwmo:LDT_RetourCode",
            "@maxOccurs": "unbounded",
            "xs:annotation": {
              "xs:documentation": "Gecodeerde aanduiding in een retourbericht van het resultaat van de beoordeling van een (deel van het) ontvangen bericht."
            }
          }
        }
      },
      {
        "@name": "Antwoord",
        "xs:annotation": {
          "xs:documentation": "Gegevens over het antwoord dat de gemeente geeft op een verzoek van de aanbieder."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "ReferentieAanbieder",
              "@type": "iwmo:LDT_Referentie"
            },
            {
              "@name": "VerzoekAntwoord",
              "@type": "iwmo:LDT_VerzoekAntwoord",
              "xs:annotation": {
                "xs:documentation": "Het antwoord op een verzoek."
              }
            },
            {
              "@name": "RedenAfwijzingVerzoek",
              "@type": "iwmo:LDT_RedenAfwijzingVerzoek",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "De reden waarom een verzoek van een zorgaanbieder wordt afgewezen door de gemeente."
              }
            },
            {
              "@name": "RetourCodes",
              "@type": "wmo320:RetourCodes"
            }
          ]
        }
      }
    ]
  }
}
