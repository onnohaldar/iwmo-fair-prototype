{
  "xs:schema": {
    "@xmlns:xs": "http://www.w3.org/2001/XMLSchema",
    "@xmlns:iwmo": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
    "@xmlns:wmo315": "http://www.istandaarden.nl/iwmo/3_0/wmo315/schema",
    "@targetNamespace": "http://www.istandaarden.nl/iwmo/3_0/wmo315/schema",
    "@elementFormDefault": "qualified",
    "xs:import": {
      "@namespace": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
      "@schemaLocation": "basisschema.xsd"
    },
    "xs:annotation": {
      "xs:appinfo": {
        "iwmo:standaard": "iwmo",
        "iwmo:bericht": "wmo315",
        "iwmo:release": "3.0",
        "iwmo:BerichtXsdVersie": "1.0.2",
        "iwmo:BerichtXsdMinVersie": "1.0.0",
        "iwmo:BerichtXsdMaxVersie": "1.0.2",
        "iwmo:BasisschemaXsdVersie": "1.2.0",
        "iwmo:BasisschemaXsdMinVersie": "1.0.0",
        "iwmo:BasisschemaXsdMaxVersie": "1.2.0"
      }
    },
    "xs:element": {
      "@name": "Bericht",
      "@type": "wmo315:Root"
    },
    "xs:complexType": [
      {
        "@name": "Root",
        "xs:annotation": {
          "xs:documentation": "Bericht voor het aanvragen van een toewijzing voor Wmo-ondersteuning."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Header",
              "@type": "wmo315:Header"
            },
            {
              "@name": "Client",
              "@type": "wmo315:Client"
            }
          ]
        }
      },
      {
        "@name": "Header",
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "BerichtCode",
              "xs:annotation": {
                "xs:documentation": "Code ter identificatie van een soort bericht."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtCode",
                  "xs:pattern": {
                    "@value": "444"
                  }
                }
              }
            },
            {
              "@name": "BerichtVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een major release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtVersie",
                  "xs:pattern": {
                    "@value": "3"
                  }
                }
              }
            },
            {
              "@name": "BerichtSubversie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een minor release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtSubversie",
                  "xs:pattern": {
                    "@value": "0"
                  }
                }
              }
            },
            {
              "@name": "Afzender",
              "@type": "iwmo:LDT_AgbCode",
              "xs:annotation": {
                "xs:documentation": "Identificerende code van een aanbieder van zorg of ondersteuning."
              }
            },
            {
              "@name": "Ontvanger",
              "@type": "iwmo:LDT_Gemeente",
              "xs:annotation": {
                "xs:documentation": "Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of ondersteuning."
              }
            },
            {
              "@name": "BerichtIdentificatie",
              "@type": "iwmo:CDT_BerichtIdentificatie",
              "xs:annotation": {
                "xs:documentation": "Naam of nummer en dagtekening ter identificatie van een totale aanlevering."
              }
            },
            {
              "@name": "XsdVersie",
              "@type": "iwmo:CDT_XsdVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het heenbericht."
              }
            }
          ]
        }
      },
      {
        "@name": "Client",
        "xs:annotation": {
          "xs:documentation": "Persoonsgegevens van de client."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Bsn",
              "@type": "iwmo:LDT_BurgerServicenummer",
              "xs:annotation": {
                "xs:documentation": "Een door de overheid toegekend identificerend nummer in het kader van het vereenvoudigen van het contact tussen overheid en burgers en het verminderen van de administratieve lasten."
              }
            },
            {
              "@name": "Geboortedatum",
              "@type": "iwmo:CDT_Geboortedatum",
              "xs:annotation": {
                "xs:documentation": "Datum waarop een natuurlijk persoon geboren is."
              }
            },
            {
              "@name": "Geslacht",
              "@type": "iwmo:LDT_Geslacht",
              "xs:annotation": {
                "xs:documentation": "De sekse van een persoon, zoals bij geboorte formeel vastgesteld of nadien formeel gewijzigd."
              }
            },
            {
              "@name": "Naam",
              "@type": "iwmo:CDT_VerkorteNaam",
              "xs:annotation": {
                "xs:documentation": "De achternaam van een natuurlijk persoon, aangeduid als Naam en Voorvoegsel."
              }
            },
            {
              "@name": "Commentaar",
              "@type": "iwmo:LDT_Commentaar",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Vrije tekst (bijvoorbeeld toelichting) in een bericht."
              }
            },
            {
              "@name": "AangevraagdeProducten",
              "@type": "wmo315:AangevraagdeProducten"
            }
          ]
        }
      },
      {
        "@name": "AangevraagdeProducten",
        "xs:sequence": {
          "xs:element": {
            "@name": "AangevraagdProduct",
            "@type": "wmo315:AangevraagdProduct",
            "@maxOccurs": "unbounded"
          }
        }
      },
      {
        "@name": "AangevraagdProduct",
        "xs:annotation": {
          "xs:documentation": "Gegevens over het product waarvoor een toewijzing wordt aangevraagd."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "ReferentieAanbieder",
              "@type": "iwmo:LDT_Referentie",
              "xs:annotation": {
                "xs:documentation": "Naam of nummer die als referentie kan worden meegegeven."
              }
            },
            {
              "@name": "BeschikkingNummer",
              "@type": "iwmo:LDT_Nummer",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Identificerend nummer van een beschikking zoals vastgesteld door de gemeente."
              }
            },
            {
              "@name": "Product",
              "@type": "iwmo:CDT_Product",
              "xs:annotation": {
                "xs:documentation": "Gecodeerde omschrijving van een product, dienst of resultaat ten behoeve van het leveren van ondersteuning aan een client."
              }
            },
            {
              "@name": "BeschikkingIngangsdatum",
              "@type": "iwmo:LDT_Datum",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Ingangsdatum van een afgegeven beschikking."
              }
            },
            {
              "@name": "ToewijzingIngangsdatum",
              "@type": "iwmo:LDT_Datum",
              "xs:annotation": {
                "xs:documentation": "De aangevraagde Ingangsdatum van het toe te wijzen Product."
              }
            },
            {
              "@name": "ToewijzingEinddatum",
              "@type": "iwmo:LDT_Datum",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "De aangevraagde Einddatum van het toe te wijzen Product."
              }
            },
            {
              "@name": "Omvang",
              "@type": "iwmo:CDT_Omvang",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Aanduiding van de omvang van de te leveren of geleverde ondersteuning, uitgedrukt in Volume, Eenheid en Frequentie."
              }
            },
            {
              "@name": "Verwijzer",
              "@type": "iwmo:CDT_Verwijzer",
              "xs:annotation": {
                "xs:documentation": "Gegevens over de persoon of instantie die een client heeft doorverwezen naar ondersteuning, aangeduid als Type en Naam of ZorgverlenerCode."
              }
            },
            {
              "@name": "Raamcontract",
              "@type": "iwmo:LDT_JaNee",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Indicator of de aanbieder voor de te leveren ondersteuning beroep doet op een landelijk raamcontract."
              }
            },
            {
              "@name": "Commentaar",
              "@type": "iwmo:LDT_Commentaar",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Vrije tekst (bijvoorbeeld toelichting) in een bericht."
              }
            }
          ]
        }
      }
    ]
  }
}
